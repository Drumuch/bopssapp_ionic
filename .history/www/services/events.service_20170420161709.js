(function () {
    'use strict';

    angular
        .module('app')
        .factory('events', events);

    events.$inject = ['$resource', 'config'];

    function events($resource, config) {

        return {
            getEventDays: getEventDays
        }

        function getEventDays() {
            return $resource(
                config.server + 'event-days/:id',
                {
                    cache: true,
                    headers: config.auth,
                },
                {
                    getOne: {
                        method: "GET",
                        headers: config.auth,
                        params: {
                            id: '@id'
                        },
                        isArray: true,
                        cache: true
                    }
                }
            )
        }


    }
})();
