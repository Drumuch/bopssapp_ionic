(function () {
    'use strict';

    angular
        .module('app')
        .factory('serverConfigs', serverConfigs);

    serverConfigs.$inject = ['$resource', 'config'];

    function serverConfigs($resource, config) {

        return $resource(config.server + 'configs', {}, { isArray: true });

    }
})();
