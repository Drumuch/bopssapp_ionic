(function () {
    'use strict';

    angular
        .module('app')
        .factory('pages', pages);

    pages.$inject = ['$resource', 'config'];

    function pages($resource, config) {

        return {
            getPage: getPage
        }

        function getPage() {
            return $resource(config.server + 'wp-json/wp/v2/pages/:id')
        }


    }
})();
