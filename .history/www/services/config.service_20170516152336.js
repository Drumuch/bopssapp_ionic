(function () {
    'use strict';

    angular
        .module('app')
        .factory('appConfigs', appConfigs);

    appConfigs.$inject = ['$resource', 'config'];

    function appConfigs($resource, config) {

        return $resource(config.server + 'configs');

    }
})();
