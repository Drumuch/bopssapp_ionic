(function () {
    'use strict';

    angular
        .module('app')
        .factory('speakers', speakers);

    speakers.$inject = ['$resource', 'config'];

    function speakers($resource, config) {

        return {
            getSpeakers: getSpeakers
        }

        function getSpeakers() {
            return $resource(
                config.server + 'authors/:id',
                {},
                {
                    getOne: {
                        method: 'GET',
                        headers: config.auth,
                        params: {
                            id: '@id'
                        },
                        isArray: true,
                        cache: true
                    },
                    query: {
                        method: 'GET',
                        headers: config.auth,
                        params: {
                            category_name: '2017'
                        },
                        isArray: false,
                        cache: true
                    }
                }
            )
        }


    }
})();
