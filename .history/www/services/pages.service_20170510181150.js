(function () {
    'use strict';

    angular
        .module('app')
        .factory('pages', pages);

    pages.$inject = ['$resource', 'config'];

    function pages($resource, config) {

        return {
            getPage: getPage
        }

        function getPage() {
            return $resource(config.server + 'pages/:id',
                {},
                {
                    get: {
                        method: 'GET',
                        headers: config.auth,
                        isArray: true,
                        cache: true
                    }
                })
        }


    }
})();
