(function () {
    'use strict';

    angular
        .module('app')
        .factory('speakers', speakers);

    speakers.$inject = ['$resource', 'config'];

    function speakers($resource, config) {

        return {
            getSpeakers: getSpeakers
        }

        function getSpeakers(_id) {
            console.log(_id)
            return $resource(
                config.server + 'authors/:id',
                {},
                {
                    getOne: {
                        method: 'GET',
                        url: config.server + 'authors/' + _id,
                        headers: config.auth,
                        params: {
                            id: '@id'
                        },
                        isArray: false,
                        cache: true
                    },
                    query: {
                        method: 'GET',
                        headers: config.auth,
                        isArray: false,
                        cache: true
                    }
                }
            )
        }


    }
})();
