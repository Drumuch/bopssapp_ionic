(function () {
    'use strict';

    angular
        .module('app')
        .factory('appConfigs', appConfigs);

    appConfigs.$inject = ['$resource', 'config'];

    function appConfigs($resource, config) {

        return $resource(
            config.server + 'event-days/:id',
            {},
            {
                getOne: {
                    method: 'GET',
                    headers: config.auth,
                    params: {
                        id: '@id'
                    },
                    isArray: true,
                    cache: true
                },
                query: {
                    method: 'GET',
                    headers: config.auth,
                    isArray: true,
                    cache: true
                }
            }
        )

    }
})();
