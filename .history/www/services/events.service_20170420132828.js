(function () {
    'use strict';

    angular
        .module('app')
        .factory('events', events);

    events.$inject = ['$state', '$resource', 'config'];

    function events($state, $resource, config) {

        return {
            getEventDays: getEventDays
        }

        function getEventDays() {
            return $resource(
                // 'http://bopss/wp-json/wp/v2/phrase',
                'https://ne2.bopss.co.uk/wp-json/wp/v2/phrase',
                // 'https://www.bopss.co.uk/wp-json/wp/v2/types',
                // root,
                {},
                {
                    getFriends: {
                        method: "GET",
                        headers: { 'Authorization': 'Basic bmljZTpob21l' },
                        isArray: true
                        // cache: true
                    }
                }
            )
        }

    }
})();
