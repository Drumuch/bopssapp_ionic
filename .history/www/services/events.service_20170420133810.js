(function () {
    'use strict';

    angular
        .module('app')
        .factory('events', events);

    events.$inject = ['$resource', 'config'];

    function events($resource, config) {
        getEventDays();

        return {
            getEventDays: getEventDays
        }

        function getEventDays() {
            return $resource(
                config.server + 'phrase',
                {},
                {
                    get: {
                        method: "GET",
                        headers: config.auth,
                        isArray: true
                        // cache: true
                    }
                }
            )
        }

    }
})();
