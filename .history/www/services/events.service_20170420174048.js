(function () {
    'use strict';

    angular
        .module('app')
        .factory('events', events);

    events.$inject = ['$resource', 'config'];

    function events($resource, config) {

        return {
            getEventDays: getEventDays
        }

        function getEventDays() {
            return $resource(
                config.server + 'event-days/:id',
                {},
                {
                    getOne: {
                        method: 'GET',
                        headers: config.auth,
                        params: {
                            id: '@id'
                        },
                        isArray: true,
                        cache: true
                    },
                    query: {
                        method: 'GET',
                        headers: config.auth,
                        isArray: true,
                        cache: true
                    }
                }
            )
        }


    }
})();
