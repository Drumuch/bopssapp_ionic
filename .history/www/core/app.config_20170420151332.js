(function () {
    'use strict';

    angular
        .module('app')
        .run(moduleStart)
        .config(ionicConfig)
        .config(routeConfig)
        .constant('config', {
            server: 'http://bopss/wp-json/wp/v2/',
            auth: { 'Authorization': 'Basic bmljZTpob21l' }
        });

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'templates/about/about.html',
                controller: 'About',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'About',
                    iconClass: 'ion-information-circled'
                }
            })
            .state('session', {
                url: '/session',
                templateUrl: 'templates/session/all/all.session.html',
                controller: 'Session',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'BOPSS 2017 Meeting',
                    iconClass: 'ion-clock'
                },
                resolve: {
                    eventDays: function (events) {
                        return events.getEventDays().get().$promise;
                    }
                }
            })
            .state('session.day', {
                url: '/session/:id',
                templateUrl: 'templates/session/one/one.session.html',
                controller: 'OneSession',
                controllerAs: 'vm',
                settings: {
                    title: 'Event Day'
                }
            })
            .state('speakers', {
                url: '/speakers',
                templateUrl: 'templates/speakers/speakers.html',
                controller: 'Speakers',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'Speakers',
                    iconClass: 'ion-person'
                }
            })

        $urlRouterProvider.otherwise('/about');
    }

    function ionicConfig($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom'); // other values: top
        $ionicConfigProvider.backButton.text('');
    }

    function moduleStart($ionicPlatform, $rootScope, $timeout) {
        $rootScope.title = '';

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeSuccess',
            function (event, current, previous) {
                var title = angular.element(document.getElementsByClassName('title'))
                title.text(current.settings.title || '');
            }
        );
    }

})();
