(function () {
    'use strict';

    angular.module('app.core', [
        /*
         * Angular modules
         */
        'ionic', 'ngResource', 'angular-loading-bar', 'ngCordova',
        /*
         * Our reusable cross app code modules
         */
        'blocks.directives' //, 'blocks.logger', 'blocks.router',
        /*
         * 3rd Party modules
         */
        // 'ngplus's
    ]);
})();
