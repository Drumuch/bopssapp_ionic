(function () {
    'use strict';

    angular
        .module('app')
        .run(moduleStart)
        .config(ionicConfig)
        .config(routeConfig)
        .constant('config', {
            // server: 'https://ne2.bopss.co.uk/wp-json/wp/v2/',
            // server: 'http://bopss/wp-json/wp/v2/',
            server: 'https://www.bopss.co.uk/wp-json/wp/v2/',
            auth: { 'Authorization': 'Basic bmljZTpob21l' }
            // auth: { 'Authorization': 'Basic ' + btoa('nice:home') }
        })
        .directive('myMap', function ($cordovaGeolocation) {
            // directive link function
            var link = function (scope, element, attrs) {
                var map, infoWindow;
                var markers = [];
                var posOptions = { timeout: 10000, enableHighAccuracy: false };
                // map config
                var mapOptions = {
                    center: new google.maps.LatLng(51.508515, -0.125487),
                    zoom: 14,
                    scrollwheel: false
                };

                var posOptions = { timeout: 10000, enableHighAccuracy: false };
                var eventPoint = new google.maps.LatLng(51.516078, -0.147135),
                    // currentPoint = new google.maps.LatLng(50.8429, -0.1313),
                    currentPoint,
                    directionsService = new google.maps.DirectionsService,
                    directionsDisplay,
                    eventMarker,
                    currentMarker;

                element[0].addEventListener("touchmove", function (event) {
                    event.preventDefault();
                });

                // init the map
                function initMap() {
                    if (map === void 0) {
                        map = new google.maps.Map(element[0], mapOptions);
                        directionsDisplay = new google.maps.DirectionsRenderer({
                            map: map
                        });

                        $cordovaGeolocation
                            .getCurrentPosition(posOptions)
                            .then(function (position) {
                                var lat = position.coords.latitude;
                                var long = position.coords.longitude;
                                currentPoint = new google.maps.LatLng(lat, long);

                                currentMarker = new google.maps.Marker({
                                    position: currentPoint,
                                    title: 'point B',
                                    label: 'B',
                                    map: map
                                });

                                eventMarker = new google.maps.Marker({
                                    position: eventPoint,
                                    title: 'The Royal Society of Medicine',
                                    label: 'A',
                                    map: map
                                });

                                // get route from A to B
                                calculateAndDisplayRoute(directionsService, directionsDisplay, eventPoint, currentPoint);
                            }, function (err) {
                                alert('Something went wrong.Check that GPS turned on and allow this App use your location')
                                setMarker(map, eventPoint, 'The Royal Society of Medicine', 'Event will be here');
                            });
                    }
                }

                function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
                    directionsService.route({
                        origin: pointA,
                        destination: pointB,
                        travelMode: google.maps.TravelMode.DRIVING
                    }, function (response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(response);
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                }

                // place a marker
                function setMarker(map, position, title, content) {
                    var marker;
                    var markerOptions = {
                        position: position,
                        map: map,
                        title: title
                        // icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                    };

                    marker = new google.maps.Marker(markerOptions);
                    markers.push(marker); // add marker to array

                    google.maps.event.addListener(marker, 'click', function () {
                        // close window if not undefined
                        if (infoWindow !== void 0) {
                            infoWindow.close();
                        }
                        // create new window
                        var infoWindowOptions = {
                            content: content
                        };
                        infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                        infoWindow.open(map, marker);
                    });
                }

                // show the map and place some markers
                if (window.google) {
                    initMap();
                }
            };

            return {
                restrict: 'A',
                template: '<div id="gmaps"></div>',
                replace: true,
                link: link
            };
        });

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'templates/about/about.html',
                controller: 'About',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'About',
                    iconClass: 'ion-information-circled'
                }
            })
            .state('session', {
                url: '/session',
                cache: false,
                // template: '<ion-view><ion-content class="padding"><days-menu days="vm.eventDays"></days-menu><ion-nav-view></ion-nav-view></ion-content></ion-view>',
                templateUrl: 'templates/session/all/all.sessions.html',
                controller: 'AllSession',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'BOPSS 2017 Meeting',
                    iconClass: 'ion-clock'
                },
                resolve: {
                    eventDays: function (events) {
                        return events.getEventDays().query().$promise;
                    }
                }
            })
            // .state('session.all', {
            //     url: '',
            //     templateUrl: 'templates/session/all/all.sessions.html'
            // })
            .state('session.one', {
                url: '/:id',
                templateUrl: 'templates/session/one/one.session.html',
                controller: 'OneSession',
                controllerAs: 'vm',
                settings: {
                    title: 'Event Day'
                },
                resolve: {
                    dayEvents: function (events, $stateParams) {
                        return events.getEventDays().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })

            .state('speakers', {
                url: '/speakers',
                abstract: true,
                template: '<ion-nav-view></ion-nav-view>'
            })
            .state('speakers.all', {
                url: '',
                templateUrl: 'templates/speakers/all/all.speakers.html',
                controller: 'AllSpeakers',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'Speakers',
                    iconClass: 'ion-person'
                },
                resolve: {
                    speakersList: function (speakers) {
                        return speakers.getSpeakers().query().$promise;
                    }
                }
            })
            .state('speakers.one', {
                url: '/:id',
                templateUrl: 'templates/speakers/one/one.speaker.html',
                controller: 'OneSpeaker',
                controllerAs: 'vm',
                settings: {
                    title: 'Speaker',
                },
                resolve: {
                    speakerInfo: function (speakers, $stateParams) {
                        return speakers.getSpeakers().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })
            .state('pages', {
                url: '/pages/:id',
                templateUrl: 'templates/static/static.html',
                controller: 'Static',
                controllerAs: 'vm',
                settings: {
                    title: 'Static'
                },
                resolve: {
                    page: function (pages, $stateParams) {
                        return pages.getPage().get({ id: $stateParams.id }).$promise;
                    }
                }

            })
            .state('location', {
                url: '/location',
                templateUrl: 'templates/location/location.html',
                controller: 'Location',
                controllerAs: 'vm',
                settings: {
                    title: 'Locatin'
                }
            });

        $urlRouterProvider.otherwise('/about');
    }

    function ionicConfig($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom'); // other values: top
        $ionicConfigProvider.backButton.text('');
    }

    function moduleStart($ionicPlatform, $rootScope, $timeout) {
        $rootScope.title = '';

        // document.addEventListener("deviceready", onDeviceReady, false);

        // function onDeviceReady() {
        //     console.log('deviceready...')
        // }

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeSuccess',
            function (event, current, previous) {
                var title = angular.element(document.getElementsByClassName('title'))
                title.text(current.settings.title || '');
            }
        );
    }

})();