(function () {
    'use strict';

    angular
        .module('app')
        .run(moduleStart)
        .config(ionicConfig)
        .config(routeConfig)
        .constant('config', {
            // server: 'https://ne2.bopss.co.uk/wp-json/wp/v2/',
            // server: 'http://bopss/wp-json/wp/v2/',
            server: 'https://www.bopss.co.uk/wp-json/wp/v2/',
            auth: { 'Authorization': 'Basic bmljZTpob21l' }
            // auth: { 'Authorization': 'Basic ' + btoa('nice:home') }
        });

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'templates/about/about.html',
                controller: 'About',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'About',
                    iconClass: 'ion-information-circled'
                }
            })
            .state('session', {
                url: '/session',
                cache: false,
                // template: '<ion-view><ion-content class="padding"><days-menu days="vm.eventDays"></days-menu><ion-nav-view></ion-nav-view></ion-content></ion-view>',
                templateUrl: 'templates/session/all/all.sessions.html',
                controller: 'AllSession',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'BOPSS 2017 Meeting',
                    iconClass: 'ion-clock'
                },
                resolve: {
                    eventDays: function (events) {
                        return events.getEventDays().query().$promise;
                    }
                }
            })
            // .state('session.all', {
            //     url: '',
            //     templateUrl: 'templates/session/all/all.sessions.html'
            // })
            .state('session.one', {
                url: '/:id',
                templateUrl: 'templates/session/one/one.session.html',
                controller: 'OneSession',
                controllerAs: 'vm',
                settings: {
                    title: 'Event Day'
                },
                resolve: {
                    dayEvents: function (events, $stateParams) {
                        return events.getEventDays().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })

            .state('speakers', {
                url: '/speakers',
                abstract: true,
                template: '<ion-nav-view></ion-nav-view>'
            })
            .state('speakers.all', {
                url: '',
                templateUrl: 'templates/speakers/all/all.speakers.html',
                controller: 'AllSpeakers',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'Speakers',
                    iconClass: 'ion-person'
                },
                resolve: {
                    speakersList: function (speakers) {
                        return speakers.getSpeakers().query().$promise;
                    }
                }
            })
            .state('speakers.one', {
                url: '/:id',
                templateUrl: 'templates/speakers/one/one.speaker.html',
                controller: 'OneSpeaker',
                controllerAs: 'vm',
                settings: {
                    title: 'Speaker',
                },
                resolve: {
                    speakerInfo: function (speakers, $stateParams) {
                        return speakers.getSpeakers().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })
            .state('pages', {
                url: '/pages/:id',
                templateUrl: 'templates/static/static.html',
                controller: 'Static',
                controllerAs: 'vm',
                settings: {
                    title: 'Static'
                },
                resolve: {
                    page: function (pages, $stateParams) {
                        return pages.getPage().get({ id: $stateParams.id }).$promise;
                    }
                }

            })
            .state('location', {
                url: '/location',
                templateUrl: 'templates/location/location.html',
                controller: 'Location',
                controllerAs: 'vm',
                settings: {
                    title: 'Locatin'
                }
            });

        $urlRouterProvider.otherwise('/about');
    }

    function ionicConfig($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom'); // other values: top
        $ionicConfigProvider.backButton.text('');
    }

    function moduleStart($ionicPlatform, $rootScope, $timeout) {
        $rootScope.title = '';

        window.initMap = function () {
            var uluru = { lat: -25.363, lng: 131.044 };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeSuccess',
            function (event, current, previous) {
                var title = angular.element(document.getElementsByClassName('title'))
                title.text(current.settings.title || '');
            }
        );
    }

})();
