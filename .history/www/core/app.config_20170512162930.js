(function () {
    'use strict';

    angular
        .module('app')
        .run(moduleStart)
        .config(ionicConfig)
        .config(routeConfig)
        .constant('config', {
            // server: 'https://ne2.bopss.co.uk/wp-json/wp/v2/',
            // server: 'http://bopss/wp-json/wp/v2/',
            server: 'https://www.bopss.co.uk/wp-json/wp/v2/',
            auth: { 'Authorization': 'Basic bmljZTpob21l' }
            // auth: { 'Authorization': 'Basic ' + btoa('nice:home') }
        })
        .directive('myMap', function () {
            // directive link function
            var link = function (scope, element, attrs) {
                var map, infoWindow;
                var markers = [];

                // map config
                var mapOptions = {
                    center: new google.maps.LatLng(51.508515, -0.125487),
                    zoom: 13,
                    scrollwheel: false,
                    // draggable: !("ontouchend" in document)
                };

                element[0].addEventListener("touchmove", function (event) {
                    event.preventDefault();
                });

                // init the map
                function initMap() {
                    if (map === void 0) {
                        // map = new google.maps.Map(element[0], mapOptions);
                        var pointA = new google.maps.LatLng(51.7519, -1.2578),
                            pointB = new google.maps.LatLng(50.8429, -0.1313),
                            myOptions = {
                                zoom: 7,
                                center: pointA
                            },
                            // map = new google.maps.Map(document.getElementById('map-canvas'), myOptions),
                            map = new google.maps.Map(element[0], mapOptions),
                            // Instantiate a directions service.
                            directionsService = new google.maps.DirectionsService,
                            directionsDisplay = new google.maps.DirectionsRenderer({
                                map: map
                            }),
                            markerA = new google.maps.Marker({
                                position: pointA,
                                title: "point A",
                                label: "A",
                                map: map
                            }),
                            markerB = new google.maps.Marker({
                                position: pointB,
                                title: "point B",
                                label: "B",
                                map: map
                            });

                        // get route from A to B
                        calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);
                        var infoWindow = new google.maps.InfoWindow({ map: map });
                        // Try HTML5 geolocation.
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function (position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                infoWindow.setPosition(pos);
                                infoWindow.setContent('Location found.');
                                map.setCenter(pos);
                            }, function () {
                                handleLocationError(true, infoWindow, map.getCenter());
                            });
                        } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                        }
                    }
                }

                function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                    infoWindow.setPosition(pos);
                    infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
                }

                function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
                    directionsService.route({
                        origin: pointA,
                        destination: pointB,
                        travelMode: google.maps.TravelMode.DRIVING
                    }, function (response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(response);
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                }


                // place a marker
                function setMarker(map, position, title, content) {
                    var marker;
                    var markerOptions = {
                        position: position,
                        map: map,
                        title: title
                        // icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                    };

                    marker = new google.maps.Marker(markerOptions);
                    markers.push(marker); // add marker to array

                    google.maps.event.addListener(marker, 'click', function () {
                        // close window if not undefined
                        if (infoWindow !== void 0) {
                            infoWindow.close();
                        }
                        // create new window
                        var infoWindowOptions = {
                            content: content
                        };
                        infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                        infoWindow.open(map, marker);
                    });
                }

                // show the map and place some markers
                initMap();

                // setMarker(map, new google.maps.LatLng(51.508515, -0.125487), 'London', 'Just some content');
            };

            return {
                restrict: 'A',
                template: '<div id="gmaps"></div>',
                replace: true,
                link: link
            };
        });

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'templates/about/about.html',
                controller: 'About',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'About',
                    iconClass: 'ion-information-circled'
                }
            })
            .state('session', {
                url: '/session',
                cache: false,
                // template: '<ion-view><ion-content class="padding"><days-menu days="vm.eventDays"></days-menu><ion-nav-view></ion-nav-view></ion-content></ion-view>',
                templateUrl: 'templates/session/all/all.sessions.html',
                controller: 'AllSession',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'BOPSS 2017 Meeting',
                    iconClass: 'ion-clock'
                },
                resolve: {
                    eventDays: function (events) {
                        return events.getEventDays().query().$promise;
                    }
                }
            })
            // .state('session.all', {
            //     url: '',
            //     templateUrl: 'templates/session/all/all.sessions.html'
            // })
            .state('session.one', {
                url: '/:id',
                templateUrl: 'templates/session/one/one.session.html',
                controller: 'OneSession',
                controllerAs: 'vm',
                settings: {
                    title: 'Event Day'
                },
                resolve: {
                    dayEvents: function (events, $stateParams) {
                        return events.getEventDays().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })

            .state('speakers', {
                url: '/speakers',
                abstract: true,
                template: '<ion-nav-view></ion-nav-view>'
            })
            .state('speakers.all', {
                url: '',
                templateUrl: 'templates/speakers/all/all.speakers.html',
                controller: 'AllSpeakers',
                controllerAs: 'vm',
                settings: {
                    tab: true,
                    title: 'Speakers',
                    iconClass: 'ion-person'
                },
                resolve: {
                    speakersList: function (speakers) {
                        return speakers.getSpeakers().query().$promise;
                    }
                }
            })
            .state('speakers.one', {
                url: '/:id',
                templateUrl: 'templates/speakers/one/one.speaker.html',
                controller: 'OneSpeaker',
                controllerAs: 'vm',
                settings: {
                    title: 'Speaker',
                },
                resolve: {
                    speakerInfo: function (speakers, $stateParams) {
                        return speakers.getSpeakers().getOne({ id: $stateParams.id }).$promise;
                    }
                }
            })
            .state('pages', {
                url: '/pages/:id',
                templateUrl: 'templates/static/static.html',
                controller: 'Static',
                controllerAs: 'vm',
                settings: {
                    title: 'Static'
                },
                resolve: {
                    page: function (pages, $stateParams) {
                        return pages.getPage().get({ id: $stateParams.id }).$promise;
                    }
                }

            })
            .state('location', {
                url: '/location',
                templateUrl: 'templates/location/location.html',
                controller: 'Location',
                controllerAs: 'vm',
                settings: {
                    title: 'Locatin'
                }
            });

        $urlRouterProvider.otherwise('/about');
    }

    function ionicConfig($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom'); // other values: top
        $ionicConfigProvider.backButton.text('');
    }

    function moduleStart($ionicPlatform, $rootScope, $timeout) {
        $rootScope.title = '';

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeSuccess',
            function (event, current, previous) {
                var title = angular.element(document.getElementsByClassName('title'))
                title.text(current.settings.title || '');
            }
        );
    }

})();