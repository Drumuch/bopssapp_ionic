(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .controller('MyTabsController', MyTabsController);

    MyTabsController.$inject = ['$scope'];

    function MyTabsController($scope) {
        var panes = $scope.panes = [];

        $scope.select = function (pane) {
            angular.forEach(panes, function (pane) {
                pane.selected = false;
            });
            pane.selected = true;
        };

        this.addPane = function (pane) {
            if (panes.length === 0) {
                $scope.select(pane);
            }
            panes.push(pane);
        };

        $(document).on("click", ".event", function (event) {
            if ($(event.target).hasClass('no-click')) {
                return;
            }

            $(this).find('article').toggleClass('display');
        });

    }
})();
