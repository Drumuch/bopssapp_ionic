(function () {
  'use strict';

  angular
    .module('blocks.directives')
    .directive('myTabs', function () {
      return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: 'MyTabsController',
        templateUrl: 'core/blocks/directives/my-tabs/my.tabs.html'
      };
    });
})();
