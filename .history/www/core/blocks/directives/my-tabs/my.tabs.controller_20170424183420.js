(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .controller('MyTabsController', MyTabsController);

    MyTabsController.$inject = ['$scope'];

    function MyTabsController($scope) {
        var panes = $scope.panes = [];

        $scope.select = function (pane) {
            angular.forEach(panes, function (pane) {
                pane.selected = false;
            });
            pane.selected = true;
        };

        this.addPane = function (pane) {
            if (panes.length === 0) {
                $scope.select(pane);
            }
            panes.push(pane);
        };

        $scope.click = function () {
            console.log(1231312319999);
        }
    }
})();
