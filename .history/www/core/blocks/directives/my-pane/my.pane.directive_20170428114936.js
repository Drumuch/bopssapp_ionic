(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    element.bind('click', function ($event) {
                        console.log($event.target.nodeName.toLowerCase() === 'i');
                    });
                },
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
