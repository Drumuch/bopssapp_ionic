(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    // element.bind('click', function ($event) {
                    //     console.log($event.target.nodeName.toLowerCase() === 'i');
                    // });
                },
                controller: function ($scope, $element) {
                    $scope.clicked = 0;
                    var vm = this;
                    vm.click = function () {
                        console.log(112121000);
                    }
                },
                controllerAs: 'vm',
                bindToController: true,
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
