(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    scope.togglePane = tabsCtrl.togglePane;
                },
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
