(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    element.bind('çlick', function ($event) {
                        console.log($event);
                    });
                },
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
