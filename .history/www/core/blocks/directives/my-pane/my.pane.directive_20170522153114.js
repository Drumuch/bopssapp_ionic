(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                controller: function () {
                    console.log($);
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    console.log(element);
                    element.bind('click', function ($event) {
                        if ($event.target.nodeName.toLowerCase() === 'i') {
                            var a = angular.element($event.target).parent().parent().find('article').toggleClass('display');
                        }
                        // var a = angular.element($event.target).parent().parent().find('article').toggleClass('display');
                        console.log($event.target.className.indexOf('event-time'));
                    });
                },
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
