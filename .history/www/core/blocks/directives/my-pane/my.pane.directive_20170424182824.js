(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('myPane', function () {
            return {
                require: '^^myTabs',
                restrict: 'E',
                transclude: true,
                scope: {
                    title: '@'
                },
                link: function (scope, element, attrs, tabsCtrl) {
                    tabsCtrl.addPane(scope);
                    scope.togglePane = tabsCtrl.togglePane;
                    elem.bind('click', function () {
                        var src = elem.find('img').attr('src');

                        // call your SmoothZoom here
                        angular.element(attrs.options).css({ 'background-image': 'url(' + scope.item.src + ')' });
                    });
                },
                templateUrl: 'core/blocks/directives/my-pane/my.pane.html'
            };
        });

})();
