(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('daysMenu', daysMenu);

    /* @ngInject */
    function daysMenu($state) {
        var directive = {
            link: link,
            scope: {
                days: '='
            },
            templateUrl: 'core/blocks/directives/days-menu/days.menu.html',
            restrict: 'AE'
        };

        return directive;

        function link(scope, element, attrs) {

            // if (scope.days.length) {
            //     if ($state.current.name !== 'session.one') {
            //         $state.go('session.one', { id: scope.days[0].id })
            //     }
            // }

        }
    }
})();
