(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('daysMenu', daysMenu);

    /* @ngInject */
    function daysMenu($state) {
        var directive = {
            link: link,
            scope: {},
            templateUrl: 'core/blocks/directives/days-menu/days.menu.html',
            restrict: 'AE'
        };

        return directive;

        function link(scope, element, attrs) {
            var allStates = $state.get();

            scope.tabs = allStates.filter(function (state) {
                return state.settings && state.settings.dayTab
            })

            console.log(scope.tabs)

        }
    }
})();
