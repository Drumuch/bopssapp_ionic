(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('daysMenu', daysMenu);

    /* @ngInject */
    function daysMenu($state) {
        var directive = {
            link: link,
            scope: {
                days: '='
            },
            templateUrl: 'core/blocks/directives/days-menu/days.menu.html',
            restrict: 'AE'
        };

        return directive;

        function link(scope, element, attrs) {

            console.log('121212121212');

            if (scope.days.length) {
                if ($state.current.name !== 'session.one') {
                    console.log('43434343');
                    $state.go('session.one', { id: scope.days[0].id })
                }
            }

        }
    }
})();
