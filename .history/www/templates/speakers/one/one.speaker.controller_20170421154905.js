(function () {
    'use strict';

    angular
        .module('app.speakers')
        .controller('OneSpeaker', OneSpeaker);

    OneSpeaker.$inject = ['speakerInfo'];

    function OneSpeaker(speakerInfo) {

        var vm = this;
        vm.speaker = speakerInfo;

        activate();

        function activate() {
            console.log('one.. speaker');
        }
    }
})();
