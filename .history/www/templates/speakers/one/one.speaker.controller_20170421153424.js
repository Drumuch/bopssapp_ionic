(function () {
    'use strict';

    angular
        .module('app.speakers')
        .controller('OneSpeaker', OneSpeaker);

    OneSpeaker.$inject = ['$state'];

    function OneSpeaker($state) {

        var vm = this;

        activate();

        function activate() {
            console.log('one.. speaker');
        }
    }
})();
