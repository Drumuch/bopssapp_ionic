(function () {
    'use strict';

    angular
        .module('app.speakers')
        .controller('AllSpeakers', AllSpeakers);

    AllSpeakers.$inject = ['$state', 'speakersList'];

    function AllSpeakers($state, speakersList) {

        var vm = this;
        vm.speakers = speakersList.authors;

        activate();

        function activate() {
            console.log(vm.speakers);
        }
    }
})();
