(function () {
    'use strict';

    angular
        .module('app.speakers')
        .controller('Speakers', Speakers);

    Speakers.$inject = ['$state'];

    function Speakers($state) {

        var vm = this;
        vm.speakers = [
            {
                "full_name": "Hugo Henderson",
                "id": 10192,
                "last_name": "none",
                "location": "London",
                "thumb": {
                    "small": [
                        "http://bopss/bopss-uploads/Saj-Ataullah-2016_400x399-1-150x150.jpg",
                        150,
                        150,
                        true
                    ],
                    "medium": [
                        "http://bopss/bopss-uploads/Saj-Ataullah-2016_400x399-1-300x300.jpg",
                        300,
                        300,
                        true
                    ],
                    "large": [
                        "http://bopss/bopss-uploads/Saj-Ataullah-2016_400x399-1.jpg",
                        400,
                        399,
                        false
                    ]
                }
            },
            {
                "full_name": "Test author",
                "id": 10274,
                "last_name": "none",
                "location": "",
                "thumb": {
                    "small": [
                        "http://bopss/bopss-uploads/no-photo-speaker-1-150x150.png",
                        150,
                        150,
                        true
                    ],
                    "medium": [
                        "http://bopss/bopss-uploads/no-photo-speaker-1-300x300.png",
                        300,
                        300,
                        true
                    ],
                    "large": [
                        "http://bopss/bopss-uploads/no-photo-speaker-1.png",
                        400,
                        400,
                        false
                    ]
                }
            }
        ]

        activate();

        function activate() {
            console.log(vm.speakers);
        }
    }
})();
