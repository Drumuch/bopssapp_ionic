(function () {
    'use strict';

    angular
        .module('app.static')
        .controller('Static', Static);

    Static.$inject = [];

    function Static() {

        var vm = this;

        activate();

        function activate() {
            console.log('Static ... ');
        }
    }
})();
