(function () {
    'use strict';

    angular
        .module('app.static')
        .controller('Static', Static);

    Static.$inject = ['page'];

    function Static(page) {

        var vm = this;
        vm.content = page.content.rendered;

        activate();

        function activate() {
            console.log('Static ... ');
        }
    }
})();
