(function () {
    'use strict';

    angular
        .module('app.static')
        .controller('Static', Static);

    Static.$inject = ['pages'];

    function Static(pages) {

        var vm = this;
        vm.content = pages.content.rendered;

        activate();

        function activate() {
            console.log('Static ... ');
        }
    }
})();
