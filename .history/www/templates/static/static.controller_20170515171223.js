(function () {
    'use strict';

    angular
        .module('app.static')
        .controller('Static', Static);

    Static.$inject = ['page', 'config'];

    function Static(page, config) {

        var vm = this;
        vm.content = page.content.rendered.replace(/href="/g, 'href="' + config.link).replace(/src="/g, 'src="' + config.link);

        activate();

        function activate() {
            console.log('Static ... ');
        }
    }
})();
