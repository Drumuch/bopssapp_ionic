(function () {
    'use strict';

    angular
        .module('app.static')
        .controller('Static', Static);

    Static.$inject = ['page'];

    function Static(page) {

        var vm = this;
        vm.content = page;

        activate();

        function activate() {
            console.log('Static ... ');
        }
    }
})();
