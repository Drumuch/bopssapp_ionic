(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('Session', Session);

    Session.$inject = ['eventDays'];

    function Session(eventDays) {

        var vm = this;
        vm.eventDays = eventDays;

        activate();

        function activate() {
            console.log(vm.eventDays);
        }
    }
})();
