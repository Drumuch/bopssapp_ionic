(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('AllSession', AllSession);

    AllSession.$inject = ['eventDays', 'appCon'];

    function AllSession(eventDays, appCon) {

        var vm = this;
        vm.eventDays = eventDays;
        vm.toggled = (appCon['0'].schedule_collapsed == '1');

        activate();

        function activate() {
            console.log(vm.toggled);
        }
    }
})();
