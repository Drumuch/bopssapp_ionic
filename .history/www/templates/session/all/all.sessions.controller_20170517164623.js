(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('AllSession', AllSession);

    AllSession.$inject = ['eventDays', 'appCon', 'ModalService', '$scope', 'speakers'];

    function AllSession(eventDays, appCon, ModalService, $scope, speakers) {

        var vm = this;
        vm.eventDays = eventDays;
        vm.toggled = (appCon['0'].schedule_collapsed == '1');
        vm.openAuthor = openAuthor;
        activate();

        function activate() {
            console.log(vm.toggled, eventDays);
        }

        $scope.author = {
            name: "Yarik",
            year: 1888
        }
        speakers.getSpeakers().getOne().$promise.then(function (res) {
            console.log(res);
        })

        function openAuthor() {
            ModalService.showModal({
                templateUrl: 'modal.html',
                controller: "ModalController",
                scope: $scope
            }).then(function (modal) {
                // console.log(angular.element(modal.element[0]))
                // modal.element.show();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                    console.log($scope.message)
                });
            });
        }
    }
})();
