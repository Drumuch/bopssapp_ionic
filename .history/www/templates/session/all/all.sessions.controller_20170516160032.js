(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('AllSession', AllSession);

    AllSession.$inject = ['eventDays', 'appCon'];

    function AllSession(eventDays, appCon) {

        var vm = this;
        vm.eventDays = eventDays;
        vm.toggled = true;

        activate();

        function activate() {
            console.log(!vm.toggled);
            console.log(vm.eventDays[0].events[3].authors);
        }
    }
})();
