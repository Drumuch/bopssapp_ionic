(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('AllSession', AllSession);

    AllSession.$inject = ['eventDays', 'appCon', 'ModalService'];

    function AllSession(eventDays, appCon, ModalService) {

        var vm = this;
        vm.eventDays = eventDays;
        vm.toggled = (appCon['0'].schedule_collapsed == '1');
        vm.openAuthor = openAuthor;
        activate();

        function activate() {
            console.log(vm.toggled, eventDays);
        }

        function openAuthor() {
            ModalService.showModal({
                templateUrl: 'modal.html',
                controller: "ModalController"
            }).then(function (modal) {
                // console.log(angular.element(modal.element[0]))
                // modal.element.show();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                    console.log($scope.message)
                });
            });
        }
    }
})();
