(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('AllSession', AllSession);

    AllSession.$inject = ['eventDays'];

    function AllSession(eventDays) {

        var vm = this;
        vm.eventDays = eventDays;

        activate();

        function activate() {
            console.log(vm.eventDays.events);
        }
    }
})();
