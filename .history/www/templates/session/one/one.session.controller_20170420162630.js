(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('OneSession', OneSession);

    OneSession.$inject = ['dayEvents'];

    function OneSession(dayEvents) {

        var vm = this;
        vm.dayEvents = dayEvents;

        activate();

        function activate() {
            console.log(vm.dayEvents);
        }
    }
})();
