(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('OneSession', OneSession);

    OneSession.$inject = [];

    function OneSession() {

        var vm = this;
        vm.eventDays = [1, 2, 2];

        activate();

        function activate() {
            console.log(vm.eventDays);
        }
    }
})();
