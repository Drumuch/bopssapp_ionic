(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('OneSession', OneSession);

    OneSession.$inject = ['eventDays'];

    function OneSession(eventDays) {

        var vm = this;
        vm.eventDays = eventDays;

        activate();

        function activate() {
            console.log(vm.eventDays);
        }
    }
})();
