(function () {
    'use strict';

    angular
        .module('app.session')
        .controller('Session', Session);

    Session.$inject = ['eventDays'];

    function Session(eventDays) {

        var vm = this;

        activate();

        function activate() {
            console.log(eventDays);
        }
    }
})();
