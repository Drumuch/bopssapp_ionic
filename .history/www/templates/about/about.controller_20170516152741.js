(function () {
    'use strict';

    angular
        .module('app.about')
        .controller('About', About);

    About.$inject = ['$state', 'events'];

    function About($state, events) {

        var vm = this;

        activate();

        function activate() {
            console.log(appCon);
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {
            events.getEventDays().get().$promise.then(
                function (res) {
                    console.log(res);
                },
                function (error) {
                    // If something goes wrong with a JSONP request in AngularJS,
                    // the status code is always reported as a "0". As such, it's
                    // a bit of black-box, programmatically speaking.
                    console.log(error);
                }
            );
        }
    }
})();
