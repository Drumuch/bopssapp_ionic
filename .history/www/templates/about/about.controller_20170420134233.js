(function () {
    'use strict';

    angular
        .module('app.about')
        .controller('About', About);

    About.$inject = ['$state', 'events'];

    function About($state, events) {

        var vm = this;

        activate();

        function activate() {
            console.log($state.get());
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {
            events.getEventDays().getFriends()
            // events.getEventDays().$promise.then(
            //     function (friends) {
            //         console.log(friends);
            //     },
            //     function (error) {
            //         // If something goes wrong with a JSONP request in AngularJS,
            //         // the status code is always reported as a "0". As such, it's
            //         // a bit of black-box, programmatically speaking.
            //         console.log(error);
            //     }
            // );
        }

        // $.ajax({
        //     url: root + '/posts/1',
        //     method: 'GET'
        // }).then(function (data) {
        //     console.log(data);
        // });
    }
})();
