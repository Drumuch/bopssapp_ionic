(function () {
    'use strict';

    angular
        .module('app.about').controller('ModalController', function ($scope, close) {

            $scope.close = function (result) {
                close(result, 500); // close, but give 500ms for bootstrap to animate
            };

        })
        .controller('About', About);

    About.$inject = ['$state', 'events', '$scope', 'ModalService'];

    function About($state, events, $scope, ModalService) {

        var vm = this;

        activate();

        function activate() {
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {
            ModalService.showModal({
                templateUrl: 'modal.html',
                controller: "ModalController"
            }).then(function (modal) {
                // console.log(angular.element(modal.element[0]))
                $(modal.element).show();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                    console.log($scope.message);
                });
            });
        }
    }
})();
