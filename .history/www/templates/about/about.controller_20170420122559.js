(function () {
    'use strict';

    angular
        .module('app.about')
        .controller('About', About);

    About.$inject = ['$state', '$resource', 'config'];

    function About($state, $resource, config) {

        var vm = this;

        activate();

        function activate() {
            console.log($state.get());
        }


        var root = '//jsonplaceholder.typicode.com/posts/1';

        var resource = $resource(
            // 'http://bopss/wp-json/wp/v2/phrase',
            // 'https://ne2.bopss.co.uk/wp-json/wp/v2/phrase',
            'https://www.bopss.co.uk/wp-json/wp/v2/types',
            {
                // callback: "JSON_CALLBACK"
                // callback: "myCall"
            },
            {
                getFriends: {
                    method: "GET",
                    // headers: { 'Origin': 'https://ne2.bopss.co.uk/' },
                    isArray: true
                    // cache: true
                }
            }
        );

        function myCall(params) {
            console.log(params);
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {
            resource.getFriends().$promise.then(
                function (friends) {
                    console.log(friends);
                },
                function (error) {
                    // If something goes wrong with a JSONP request in AngularJS,
                    // the status code is always reported as a "0". As such, it's
                    // a bit of black-box, programmatically speaking.
                    console.log(error);
                }
            );
        }

        // $.ajax({
        //     url: root + '/posts/1',
        //     method: 'GET'
        // }).then(function (data) {
        //     console.log(data);
        // });
    }
})();
