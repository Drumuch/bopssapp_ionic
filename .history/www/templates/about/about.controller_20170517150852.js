(function () {
    'use strict';

    angular
        .module('app.about')
        .controller('About', About);

    About.$inject = ['$state', 'events', 'ModalService'];

    function About($state, events, ModalService) {

        var vm = this;

        activate();

        function activate() {
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {
            ModalService.showModal({
                templateUrl: 'modal.html',
                controller: "ModalController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                });
            });

        }
    }
})();
