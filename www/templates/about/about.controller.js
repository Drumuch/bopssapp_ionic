(function () {
    'use strict';

    angular
        .module('app.about').controller('ModalController', function ($scope, close) {

            $scope.close = function (result) {
                close(result, 500); // close, but give 500ms for bootstrap to animate
            };

        })
        .controller('About', About);

    About.$inject = ['$state', 'events'];

    function About($state, events) {

        var vm = this;

        activate();

        function activate() {
        }

        vm.fff = loadRemoteData;

        function loadRemoteData() {

        }
    }
})();
