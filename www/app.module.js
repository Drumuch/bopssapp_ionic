(function () {
    'use strict';

    angular.module('app', [
        'app.core',

        /*
         * Feature areas
         */
        'app.about',
        'app.session',
        'app.speakers',
        'app.static',
        'app.location'
    ]);
})();
