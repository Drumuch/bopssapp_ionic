(function () {
    'use strict';

    angular
        .module('blocks.directives')
        .directive('appFooter', appFooter);

    /* @ngInject */
    function appFooter($state) {
        var directive = {
            link: link,
            scope: {},
            templateUrl: 'core/blocks/directives/app-footer/app.footer.html',
            restrict: 'AE'
        };

        return directive;

        function link(scope, element, attrs) {
            var allStates = $state.get();

            // scope.tabs = [1, 12, 13, 15]

            scope.tabs = allStates.filter(function (state) {
                return state.settings && state.settings.tab
            })

            console.log(scope.tabs)

        }
    }
})();
